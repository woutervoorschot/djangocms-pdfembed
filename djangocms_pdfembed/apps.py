from django.apps import AppConfig


class PdfembedConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangocms_pdfembed'
